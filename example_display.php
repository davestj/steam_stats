<?php
/*
steamcast xml source stats api example display
Author: dstjohn (Mediacast1)
Date started: 04-06-2005 (1:00A.M)
NOTES:
This is an example script on how to use the this api and display stats
on your website or otherwise.
*/
//include the files below on the pages you want to display stats
include('./config.php');
include('./steam_source_stats.php');
include('./steam_global_stats.php');

/*
********************************************************************************
available steam global variables
$version
$total_nodes
$total_max_nodes
$peak_total_nodes
$total_average_connect_time
$total_tuneins
$total_extended_tuneins
$system_time
$active_sources
*/
//now we know what variables we have to work with, lets display them shall we.

echo 'Steamcast global stats<br>
steamcast server version: '.$version.'<br>
Global current listeners: '.$total_nodes.'<br>
Global max listeners: '.$total_max_nodes.'<br>
Global peak listeners: '.$peak_total_nodes.'<br>
Global average connection time: '.$total_average_connect_time.' seconds<br>
Global stream connections: '.$total_tuneins.'<br>
Global extended connections: '.$total_extended_tuneins.'<br>
System time of server: '.$system_time.'<br>
Global source connections: '.$active_sources.'<br>';


/*
available steam source variables
$mount
$status
$name
$description
$genre
$url
$bitrate
$meta_song
$meta_url
$nodes
$max_nodes
$average_connect_time
$tuneins
$extended_tuneins
$connect_time
$bytes_recv
$content_type
*/
//since we seen above on how to display the variables we wanted to use in the above
//example with the global stats, ill leave the source stats display up to you guys ;)


?>

